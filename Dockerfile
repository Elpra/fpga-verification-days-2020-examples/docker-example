ARG INSTALL_DIR=/opt/altera/13.0sp1

FROM centos:7 AS base
LABEL maintainer="Tobias Baumann <tobias.baumann@elpra.de>"
RUN yum -y update && \
	yum install -y \
		compat-libstdc++-33.i686 expat.i686 fontconfig.i686 freetype.i686 glibc.i686 \
		gtk2.i686 libcanberra-gtk2.i686 gtk2-engines-2.18.4-5.el6.centos.i686 libpng.i686 \
		libICE.i686 libSM.i686 libuuid.i686 ncurses-devel.i686 ncurses-libs.i686 && \
	yum clean all


FROM base AS quartus-build
ARG INSTALL_DIR
ADD Quartus-web-13.0.1.232-linux.tar /home
WORKDIR /home
RUN ./setup.sh --mode unattended --unattendedmodeui minimal --installdir ${INSTALL_DIR} --disable-components quartus_help,modelsim_ae,max_web,devinfo,arria_web,cyclonev && \
	sed -i 's,linux_rh60,linux,g' /opt/altera/13.0sp1/modelsim_ase/vco
RUN du -sh ${INSTALL_DIR}


FROM base
ARG INSTALL_DIR
COPY --from=quartus-build ${INSTALL_DIR} ${INSTALL_DIR}
RUN yum install -y python3 python3-pip && yum clean all
ENV PATH="${INSTALL_DIR}/quartus/bin:${INSTALL_DIR}/modelsim_ase/bin:${PATH}"
RUN pip3 install vunit-hdl==4.4.0 PyYAML
